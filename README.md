# IDS721 Individual Project 1

This project builds a Zola-based static site which holds all of the portfolio work of this course. 

## Author

Yuanzhi (Matthew) Lou

## Demo

[Live Preview](https://lyuanzhi.netlify.app)

## Demo Video

The demo video is called ```DemoVideo.webm``` and is in the root directory of this repository

## Environment Setup
1. Install zola: ```sudo apt install zola```
2. Verify zola: ```zola --version```

## Zola Commands
1. Create zola project: ```zola init projectName```
2. Run the website locally: ```zola serve```
3. Build the website: ```zola build```

## Website Content

### Templates
There are three templates used in this project for now.
- `base.html` - basic structures used in every html, global css
- `index.html` - home page
- `project.html` - project page

### Pages
There are two pages in the site for now.
- `home` - Course Description, Learning Objectives, Core Tech Stack, ans so on.
- `project` - Hyperlink for portfolio work including 14 mini-projects, 4 individual projects, and a team project. 

Note that in `project` page, the circle in front of the project represents whether the project is completed or not, solid means completed and hollow means not completed.

## Build and Deploy Site on Push
1. Create ```.gitlab-ci.yml``` in the root directory of the repository
```
stages:
  - deploy

default:
  image: debian:stable-slim
  tags:
    - docker

variables:
  # The runner will be able to pull your Zola theme when the strategy is
  # set to "recursive"
  GIT_SUBMODULE_STRATEGY: "recursive"

  # If you don't set a version here, your site will be built with the latest
  # version of Zola available in GitHub releases.
  # Use the semver (x.y.z) format to specify a version. For example: "0.17.2" or "0.18.0".
  ZOLA_VERSION:
    description: "The version of Zola used to build the site."
    value: "0.18.0"

pages:
  stage: deploy
  script:
    - |
      apt-get update --assume-yes && apt-get install --assume-yes --no-install-recommends wget ca-certificates
      if [ $ZOLA_VERSION ]; then
        zola_url="https://github.com/getzola/zola/releases/download/v$ZOLA_VERSION/zola-v$ZOLA_VERSION-x86_64-unknown-linux-gnu.tar.gz"
        if ! wget --quiet --spider $zola_url; then
          echo "A Zola release with the specified version could not be found.";
          exit 1;
        fi
      else
        github_api_url="https://api.github.com/repos/getzola/zola/releases/latest"
        zola_url=$(
          wget --output-document - $github_api_url |
          grep "browser_download_url.*linux-gnu.tar.gz" |
          cut --delimiter : --fields 2,3 |
          tr --delete "\" "
        )
      fi
      wget $zola_url
      tar -xzf *.tar.gz
      ./zola build

  artifacts:
    paths:
      # This is the directory whose contents will be deployed to the GitLab Pages
      # server.
      # GitLab Pages expects a directory with this name by default.
      - public

  rules:
    # This rule makes it so that your website is published and updated only when
    # you push to the default branch of your repository (e.g. "master" or "main").
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```
2. Get the URL at ```Deploy > Pages```: 
```https://yl954-individual-project-1-dukeaiml-ids721-7cb240eca971d7d484ed.gitlab.io```

## Host on Netlify
1. Create ```netlify.toml``` in the root directory of the repository
```
[build]
publish = "/public"
command = "zola build"

[build.environment]
ZOLA_VERSION = "0.18.0"

[context.deploy-preview]
command = "zola build --base-url $DEPLOY_PRIME_URL"
```
2. Link ```Netlify``` with GitLab, and deploy this project on Netlify
3. Here is the URL: ```https://lyuanzhi.netlify.app```

## screenshot

### Deploy on GitLab

![](./screenshots/deploy.png)

### Host on Netlify

![](./screenshots/host.png)

### Home

![](./screenshots/home_top.png)

![](./screenshots/home_bottom.png)

### Project

![](./screenshots/project.png)

### About Me

![](./screenshots/about.png)

![](./screenshots/piano.png)
